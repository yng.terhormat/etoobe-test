<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ModCus;

class ConCus extends Controller {

	public function home(Request $req) {
		$prm = array();
		$prm['data'] = $this->view();
		$prm['pagination'] = view('pagination', $prm);

		$prm['child'] = view('childs.home', $prm);
		
	return view('index-', $prm);
	}

	public function send(Request $request) {
		$keyword = $request->input('keyword'); //echo "[$keyword]";
		switch ($keyword) {
			case 'insert':
				$str = strtoupper($request->input('key')); //"CUT MINI 28 BANDA ACEH";
				$name = $this->checkname($str);
				$age = $this->checkage($name, $str);

				$arr['name'] = $name['string'];
				$arr['age'] = $age['age'];
				$arr['city'] = $age['city']; //var_dump($arr);
				$id = ModCus::c_new($arr, 1); 
				if ($id) {
					$arr['code'] = 4;
					$arr['status'] = "inserted";
				} else $arr['code'] = 2;

				return $arr;
			break;
			
			default:
				# code...
				break;
		}/**/
	}
	private function checkname($str) {
		$res = array();
		$ori = $str;

		$loop = true;
		$i = 0;
		$finded = false;
		while ($loop) {
			$char = substr($str, 0, 1);
			if (is_numeric($char)) {
				$finded = true;
				$loop = false;
			}

			$str = substr($str, 1);

			if ($i==20) $loop = false;
			if (!$finded) $i++;
		}
		$res['string'] = substr($ori, 0, $i-1);
		$res['index'] = $i;

	return $res;
	}
	private function checkage($arr, $string) { //var_dump($arr);
		$res = array();
		$str = substr($string, $arr['index']); //echo $str."<br>";
		$ori = $string;

		$loop = true;
		$i = $arr['index'];
		$finded = false;
		while ($loop) {
			$char = substr($str, 0, 1);
			if (!is_numeric($char)) {
				$finded = true;
				$loop = false;
			}

			$str = substr($str, 1);

			if ($i==20) $loop = false;
			if (!$finded) $i++;
		}
		//echo $ori."<br>";
		//echo "$i ".substr($ori, $arr['index'], ($i - $arr['index']))."<br>";

		$res['age'] = substr($ori, $arr['index'], ($i - $arr['index']));
		if (strpos($ori, "TAHUN")==true) { //echo "ya TAHUN"; //return false; //echo "no"; //return false;
			$n = strpos($ori, "TAHUN");
			$res['city'] = substr($ori, ($n + strlen("TAHUN") ));
		} else if (strpos($ori, "THN")==true) { //echo "ya TAHUN"; //return false; //echo "no"; //return false;
			$n = strpos($ori, "THN");
			$res['city'] = substr($ori, ($n + strlen("THN") ));
		} else if (strpos($ori, "TH")==true){ //echo "ya TAHUN"; //return false; //echo "no"; //return false;
			$n = strpos($ori, "TH");
			$res['city'] = substr($ori, ($n + strlen("TH") ));
		} else $res['city'] = substr($ori, ($i + 1) );

	return $res;
	}
	private function view() {
		$patient = ModCus::orderby('id', 'desc')->paginate(5);
		return $patient->withPath('/');
	}
}
