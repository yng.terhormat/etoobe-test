<div class="row">
	<div class="col-12 col-s-12">
		
		<form id="Ftask" name="Ftask">
			<!--<label class="w3-text-blue">Input T:</label>-->
			<textarea id="key" name="key" placeholder="CUT MINI 28 BANDA ACEH" style="width: 100%">CUT MINI 28 TAHUN BANDA ACEH</textarea>
      <!--<input class="step-input w3-border" type="text" id="txtT" name="txtT" placeholder="T" value="1">-->
			<br><br>

			<input type="button" id="save" name="save" value="Save">
		</form>
		<br>

		<!--<div id="pagination" class="w3-center">
		</div>
		<div class="scrollable"> 
			<table id="tbl-patient" class="table-adminer" style="width: 100%">
				<thead>
					<tr>
						<th>Medical Records Id</th>
						<th>Name</th>
						<th>Address</th>
						<th>Birthday</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>-->

		Output :
		<div id="pagination" class="w3-center">
		{!! $pagination !!}
		</div>

		<div id="rescus" class="scrollable"> <!--  style="overflow-y: scroll;max-height: 100px;" -->
			<table id="tbl-cust" class="table-adminer" style="width: 100%">
				<thead>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Age</th>
						<th>City</th>
						<th>Created</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data as $key => $r)
					<tr>
						<td>{{ $r->id }}</td>
						<td>{{ $r->name }}</td>
						<td>{{ $r->age }}</td>
						<td>{{ $r->city }}</td>
						<td>{{ date("d-m-Y H:i:s", strtotime($r->created_at)) }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>
		<!--<br>-->
		
	</div>
</div>

<script type="text/javascript">
	$("#save").click(function () {
		var prm = 'keyword=insert&' + $('#Ftask').serialize(); 
		//if (constraints()) {
			console.log('send' + '-' + prm);
			_post('send', prm, 'cus|rescus');
		//}
		//resviews(prm);

	})

	function rescus(id, res) { //{"code":4,"task":{"result":3,"data":[3,6,9],"input":{"a":"1","b":"10","k":"3"}}}
		var str = 'Case ';
		console.log(res);
		/*str += res.task.input.t + ': ' + res.task.result;
		$('#' + id).append('<div>' + str + '</div>');
		$('#txtT').val(res.task.next);*/
	}
	function constraints() {
		var res = true;

		if (parseInt($('#txtT').val())<1 || parseInt($('#txtT').val())>parseInt(100)) {
			alert('Input T, more than 1 and less than 100');
			$('#txtT').focus();
			$('#txtT').select();
			res = false;
		} else if (parseInt($('#txtA').val())<1 || parseInt($('#txtA').val())>parseInt($('#txtB').val()) ) {
			alert('Input A, more than 1 and less than B' + $('#txtA').val());
			$('#txtA').focus();
			$('#txtA').select();
			res = false;
		} else if (parseInt($('#txtB').val())<parseInt($('#txtA').val()) || parseInt($('#txtB').val())>parseInt(10000)) {
			alert('Input B, more than A and less than 10000');
			$('#txtB').focus();
			$('#txtB').select();
			res = false;
		} else if (parseInt($('#txtK').val())<1 || parseInt($('#txtK').val())>parseInt(10000)) {
			alert('Input K, more than 1 and less than 10000');
			$('#txtK').focus();
			$('#txtK').select();
			res = false;
		}

	return res;
	}
</script>