@if ($data->lastPage() > 1)
	<div class="w3-bar">
		<a href="{{ $data->url(1) }}" class="w3-button" >First</a>
		<a href="{{ ($data->currentPage() == 1) ? 'javascript:void(0)' : $data->url($data->currentPage()-1) }}" class="w3-button" >&laquo;</a>

			@php
			$limit = 2;
			$pgfirst = (($data->currentPage()-$limit) <= 0) ? 1 : $data->currentPage()-$limit;
			$pg_last = (($data->currentPage()+$limit) >= $data->lastPage() ) ? $data->lastPage() : $data->currentPage()+$limit; 
			@endphp 
			<!--{{ $pgfirst."-".$pg_last."-".$data->lastPage()."-".$data->currentPage() }}-->

			@for ($i = $pgfirst; $i <= $pg_last; $i++)
				@if ($i==$data->currentPage()) <a href="javascript:void(0)" class="w3-button w3-blue">{{ $i }}</a>
				@else <a href="{{ $data->url($i) }}" class="w3-button">{{ $i }}</a> 
				@endif

			@endfor
		<a href="{{ ($data->currentPage() == $data->lastPage()) ? 'javascript:void(0)' : $data->url($data->currentPage()+1) }}" class="w3-button">&raquo;</a>
		<a href="{{ $data->url($data->lastPage()) }}" class="w3-button">Last</a>
	</div>

@endif

<!--<div id="pagination" class="w3-center">
</div>-->